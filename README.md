Perl extensions for the rxvt-unicode terminal emulator. Allows to use keyboard
shortcuts to select and copy text.

Installation
------------
Simply place the script in /usr/lib/urxvt/perl/ for system-wide availability or
in ~/.urxvt/ext/ for user-only availability.  You can also put them in a folder
of your choice, but then you have to add this line to your .Xdefaults/.Xresources:

    URxvt.perl-lib: /your/folder/

After installing, put the following lines in your .Xdefaults/.Xresources:

    URxvt.perl-ext-common: ...,keyboard-select
    URxvt.keysym.M-Escape: perl:keyboard-select:activate

Configuration
-------------
Set these values if you wish:

    URxvt.keyboard-select.clipboard: If true, copy to clipboard too
    URxvt.keyboard-select.incsearch: If true, search is performed incrementally

The following line overwrites the default Meta-s binding and allows to activate
keyboard-select directly in backward search mode:

    URxvt.keysym.M-s: perl:keyboard-select:search

Number of tabs is hardcoded to 8 but it can be changed easily in this line by
replacing both 8s:

    while ($ltxt =~ s/^( *)\t/$1 . " " x (8 - length($1) % 8)/e) {}

Usage
-----
Use Meta-Escape to activate selection mode, then use the following keys:

    h/j/k/l:           Move cursor left/down/up/right (also with arrow keys)
    g/G/0/|/^/$/-/+/H/M/L/f/F/t/T/;/,/w/W/b/B/e/E: More vi-like cursor movement keys
    '/'/?:             Start forward/backward search
    n/N:               Repeat last search, N: in reverse direction
    Ctrl-f/b:          Scroll down/up one screen
    Ctrl-d/u:          Scroll down/up half a screen
    Ctrl-e/y:          Scroll down/up one line
    v/V/Ctrl-v:        Toggle normal/linewise/blockwise selection
    Return/space:      Start selection or end selection
    y:                 Start selection from the current line
    Y:                 Copy whole lines
    q/Escape:          Quit keyboard selection mode

Commands can be preceded by a count like in vi.


